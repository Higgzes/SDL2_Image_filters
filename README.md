# Simple graphics library with animated filters (shaders) demonstration

Implementation of rendering simple geometric shapes using pure SDL2 and shaders(live-filters) for all rendering staff<br/>
The project is also include Tensor-math and Raster-Fonts implementation

### Dependencies

SDL2<br/>
STB image library to read image data<br/>
A compiler with C++17 support is required<br/>
You can install dependencies with following commands (Debian/Ubuntu):
```bash
sudo apt install libsdl2-dev libstb-dev cmake g++
```
### Usage

Click on filter name to switch between shaders.<br/>

### Building

The code can be built using cmake (tested on Linux)
```bash
git clone https://codeberg.org/Higgzes/SDL2_Image_filters.git
cd SDL2_Image_filters
mkdir build && cd build
cmake ..
make
./demo_shaders
```
### TODO
1. Increase Performance and add multithreading
2. Add OpenCV option
3. Friendly UI

### Example

![Alt text](examples/demo.png)
