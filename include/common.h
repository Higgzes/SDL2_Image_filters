#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <string_view>
#include <fstream>
#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <utility>
#include <map>
#include <vector>
#include <cmath>
#include <initializer_list>
#include <algorithm>
#include <stb/stb_image.h>
#include <stb/stb_image_write.h>
#include <stdexcept>
#include <optional>

#include <SDL2/SDL.h>


inline constexpr float zoom_value {1};
inline constexpr int C_WIDTH = 1000;
inline constexpr int C_HEIGHT = 1000;
inline constexpr int WINDOW_WIDTH = C_WIDTH * zoom_value;
inline constexpr int WINDOW_HEIGHT = C_HEIGHT * zoom_value;
