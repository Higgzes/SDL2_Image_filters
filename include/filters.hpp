#include "renderer.h"

extern dss::RGBA testShader(const dss::Renderer& renderer, int x, int y) {
    dss::RGBA finalColor{};
    const dss::RGBA& currentPixel = renderer.GetPixel(x,y);

    finalColor.r = 255 - currentPixel.r;
    finalColor.g = 255 - currentPixel.g;
    finalColor.b = 255 - currentPixel.b;

    return finalColor;
}

extern dss::RGBA grayScaleShader(const dss::Renderer& renderer, int x, int y) {
    dss::RGBA finalColor{};
    const dss::RGBA& currentPixel = renderer.GetPixel(x,y);

    finalColor.r = finalColor.g = finalColor.b =
        0.2126 * currentPixel.r + 0.7152 * currentPixel.g + 0.0722 * currentPixel.b;

    return finalColor;
}

extern dss::RGBA hsvHueRotationShader(const dss::Renderer& renderer, int x, int y) {
    dss::RGBA finalColor{};
    const dss::RGBA& currentPixel = renderer.GetPixel(x,y);

    static double v {};
    v += 0.00005;
    if(v > 360) v = 0;

    dss::HSV temp = dss::RGBtoHSV(currentPixel);
    temp.setH(temp.getH() + v);
    finalColor = dss::HSVtoRGB(temp);

    return finalColor;
}

extern dss::RGBA blurShader(const dss::Renderer& renderer, int x, int y) {
    dss::RGBA finalColor{};
    int blurSize = 3;
    int rTotal{0}, gTotal{0}, bTotal{0};
    float total = 0;

    for(int bx = -blurSize; bx <= blurSize; bx++) {
        for(int by = -blurSize; by <= blurSize; by++) {
            int fx = bx + x;
            int fy = by + y;
            if(fx < 0) fx += renderer.GetWidth();
            if(fy < 0) fy += renderer.GetWidth();
            if(fx >= 0) fx %= renderer.GetWidth();
            if(fy >= 0) fy %= renderer.GetWidth();
            rTotal += renderer.GetPixel(fx, fy).r;
            gTotal += renderer.GetPixel(fx, fy).g;
            bTotal += renderer.GetPixel(fx, fy).b;
            total++;
        }
    }
    finalColor.r = rTotal / total;
    finalColor.g = gTotal / total;
    finalColor.b = bTotal / total;

    return finalColor;
}

extern dss::RGBA hBlurShader(const dss::Renderer& renderer, int x, int y) {
    dss::RGBA finalColor{};

    int offset = -8;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.000078f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.000489f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.002403f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.009245f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.027835f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.065592f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.12098f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.17467f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.197417f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.17467f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.12098f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.065592f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.027835f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.009245f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.002403f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.000489f;
    finalColor += renderer.GetPixel(x + offset++, y) * 0.000078f;

    return finalColor;
}

extern dss::RGBA vBlurShader(const dss::Renderer& renderer, int x, int y) {
    dss::RGBA finalColor{};

    int offset = -8;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.000078f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.000489f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.002403f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.009245f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.027835f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.065592f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.12098f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.17467f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.197417f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.17467f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.12098f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.065592f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.027835f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.009245f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.002403f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.000489f;
    finalColor += renderer.GetPixel(x, y + offset++) * 0.000078f;

    return finalColor;
}

extern dss::RGBA distortionShader(const dss::Renderer& renderer, int x, int y) {
    dss::RGBA color{};

    static double v1{0};
    static double v2{0};
    static double v3{0};
    v1 = sin(v3);
    v2 = cos(v3);
    v3 += 0.0000015;
    color = renderer.GetPixel(
        x + (cos(x / 8.f + v1 * 2 * M_PI)) * 12,
        y + (sin(y / 6.f + v2 * 2 * M_PI)) * 18
        );

    return color;
}


