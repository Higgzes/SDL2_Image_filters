#include "bmp_processing.h"

dss::BaseBitmap::BaseBitmap(int width, int height, int numComponents) :
    data{new uint32_t[width * height]}, width{width}, height{height}, components{numComponents} {
    memset(data, 0, width * height * sizeof(uint32_t));
}

dss::BaseBitmap::BaseBitmap(uint32_t* source, int sourceWidth, int sourceHeight, int sourceComponents) :
    data{new uint32_t[sourceWidth * sourceHeight]}, width{sourceWidth}, height{sourceHeight}, components{sourceComponents} {
    memcpy(data, source, width * height * sizeof(uint32_t));
}

dss::BaseBitmap::BaseBitmap(std::string_view file, int reqComponents) {
    uint8_t* imageData = stbi_load(file.data(), &this->width, &this->height, &this->components, reqComponents);
    this->components = reqComponents;
    if(imageData) {
        data = new uint32_t[width * height];
        for(int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                data[i + j * width] =
                    (imageData[(i + j * width) * components + 0] << 24) +
                    (imageData[(i + j * width) * components + 1] << 16) +
                    (imageData[(i + j * width) * components + 2] <<  8) +
                    ((components == 4) ? (imageData[(i + j * width) * components + 3] <<  0) : 0xff);
            }
        }
        stbi_image_free(imageData);
    } else {
        throw std::runtime_error("Demo pict not found (" + std::string(file) + ")");
    }
}

dss::BaseBitmap::BaseBitmap(const BaseBitmap& other) :
    data{new uint32_t[other.width * other.height]}, width{other.width}, height{other.height}, components{other.components} {
    memcpy(data, other.data, width * height * sizeof(uint32_t));
}
dss::BaseBitmap& dss::BaseBitmap::operator=(const BaseBitmap& other) {
    delete[] data;
    this->width = other.width;
    this->height = other.height;
    this->components = other.components;
    data = new uint32_t[width * height];
    memcpy(data, other.data, width * height * sizeof(uint32_t));

    return *this;
}
dss::BaseBitmap::BaseBitmap(BaseBitmap&& other) noexcept :
    data{other.data} , width{other.width}, height{other.height}, components{other.components} {
    other.width = 0;
    other.height = 0;
    other.data = nullptr;
}
dss::BaseBitmap& dss::BaseBitmap::operator=(BaseBitmap&& other) noexcept {
    if(this == &other) return *this;

    delete[] data;
    this->width = other.width;
    this->height = other.height;
    this->components = other.components;
    data = other.data;
    other.width = 0;
    other.height = 0;
    other.components = 0;
    other.data = nullptr;

    return *this;
}

dss::BaseBitmap::~BaseBitmap() {
    delete[] data;
}


void dss::BaseBitmap::SaveAs(const std::string& fileName, Formats format, int quality) {
    uint32_t* abgrData = new uint32_t[this->width * this->height];
    for (int i = 0; i < this->width * this->height; i++) {
        abgrData[i] = UINT_RGBAtoUINT_ABGR(data[i]);
    }
    switch(format) {
    case Formats::PNG:
        stbi_write_png((fileName + ".png").c_str(), GetWidth(), GetHeight(), this->components, abgrData, this->components * GetWidth());
        break;
    case Formats::BMP:
        stbi_write_bmp((fileName + ".bmp").c_str(), GetWidth(), GetHeight(), this->components, abgrData);
        break;
    case Formats::TGA:
        stbi_write_tga((fileName + ".tga").c_str(), GetWidth(), GetHeight(), this->components, abgrData);
        break;
    case Formats::JPG:
        stbi_write_jpg((fileName + ".jpg").c_str(), GetWidth(), GetHeight(), this->components, abgrData, quality);
        break;
    }

    delete[] abgrData;
}

dss::RGBABitmap::RGBABitmap(int width, int height) : BaseBitmap(width, height, 4) {}
dss::RGBABitmap::RGBABitmap(uint32_t* source, int sourceWidth, int sourceHeight) : BaseBitmap(source, sourceWidth, sourceHeight, 4) {}
dss::RGBABitmap::RGBABitmap(std::string_view file) : BaseBitmap(file, 4) {}

dss::RGBABitmap::RGBABitmap(const RGBABitmap& other) : BaseBitmap(other) {}
dss::RGBABitmap& dss::RGBABitmap::operator=(const RGBABitmap& other) {
    BaseBitmap::operator=(other);
    return *this;
}
dss::RGBABitmap::RGBABitmap(RGBABitmap&& other) noexcept : BaseBitmap(other) {}
dss::RGBABitmap& dss::RGBABitmap::operator=(RGBABitmap&& other) noexcept {
    BaseBitmap::operator=(other);
    return *this;
}
dss::RGBABitmap::~RGBABitmap() {}

dss::RGBBitmap::RGBBitmap(int width, int height) : BaseBitmap(width, height, 3) {}
dss::RGBBitmap::RGBBitmap(uint32_t* source, int sourceWidth, int sourceHeight) : BaseBitmap(source, sourceWidth, sourceHeight, 4) {}
dss::RGBBitmap::RGBBitmap(std::string_view file) : BaseBitmap(file, 3) {}

dss::RGBBitmap::RGBBitmap(const RGBBitmap& other) : BaseBitmap(other) {}
dss::RGBBitmap& dss::RGBBitmap::operator=(const RGBBitmap& other) {
    BaseBitmap::operator=(other);
    return *this;
}
dss::RGBBitmap::RGBBitmap(RGBBitmap&& other) noexcept : BaseBitmap(other) {}
dss::RGBBitmap& dss::RGBBitmap::operator=(RGBBitmap&& other) noexcept {
    BaseBitmap::operator=(other);
    return *this;
}
dss::RGBBitmap::~RGBBitmap() {}
