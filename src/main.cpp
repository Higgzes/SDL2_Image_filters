#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "renderer.h"
#include "filters.hpp"

auto main() -> int
{
        SDL_Init(SDL_INIT_VIDEO);
        srand(time(NULL));

        SDL_Window* window = SDL_CreateWindow("Demo shaders_SDL2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_ALLOW_HIGHDPI);
        SDL_Renderer* renderer = SDL_CreateRenderer(window, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        SDL_Texture* texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR32, SDL_TEXTUREACCESS_STREAMING, C_WIDTH, C_HEIGHT);
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);

        std::vector<uint32_t> pixels (C_WIDTH * C_HEIGHT, 0);

        dss::Renderer Rend {pixels.data(), C_WIDTH, C_HEIGHT};
        Rend.EnableAlphaBlending();

        SDL_Event ev;
        bool is_alive {true};
        std::map<int, bool> keyboard_processing;
        int mouse_x, mouse_y;
        constexpr int shaderRectSize = 100;      

        dss::Bitmap pict{400, 400};
        dss::Renderer pict_Renderer{pict.GetData(), pict.GetWidth(), pict.GetHeight()};
        for (uint32_t x = 0; x < 400; x++) {
            for (uint32_t y = 0; y < 400; y++) {
                pict.SetRawPixel(
                    (y & (x^~y)) >> (~x&~y) | ((y & (x^~y))),
                    (x & (x^y) ) >> (~x& y) | ((x & (x^y) )),
                    (y & (x^y) ) >> ( x&~y) | ((y & (x^y) )),
                    0xff, x, y);
            }
        }

        SDL_Surface* icon = SDL_CreateRGBSurfaceFrom(
            pict.GetData(), pict.GetWidth(), pict.GetHeight(),
            32, pict.GetWidth() * sizeof(uint32_t),
            0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);

        SDL_SetWindowIcon(window, icon);
        SDL_FreeSurface(icon);

        dss::RGBA (*currentShader)(const dss::Renderer& renderer, int x, int y) {nullptr};

        dss::Bitmap triangleTexture{"./res/sample.png"};

        float val1{}, val2{};
        std::optional<SDL_Point> clicked_on {};

        while(is_alive) {
            while(SDL_PollEvent(&ev)) {
                if(ev.type == SDL_QUIT) {
                    is_alive = false;
                }
                switch(ev.type) {
                case SDL_KEYDOWN:
                    keyboard_processing[ev.key.keysym.sym] = true;
                    break;
                case SDL_KEYUP:
                    keyboard_processing[ev.key.keysym.sym] = false;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                   SDL_GetMouseState(&mouse_x, &mouse_y);
                   clicked_on = {mouse_x, mouse_y};
                   break;
                }

                SDL_GetMouseState(&mouse_x, &mouse_y);
                mouse_x /= zoom_value;
                mouse_y /= zoom_value;
            }

            if(keyboard_processing[SDLK_UP]) {
                val1 += 1;
            }
            if(keyboard_processing[SDLK_DOWN]) {
                val1 -= 1;
            }
            if(keyboard_processing[SDLK_LEFT]) {
                val2 -= 1;
            }
            if(keyboard_processing[SDLK_RIGHT]) {
                val2 += 1;
            }

            Rend.Clear(0x120F1FFF);

            SDL_Point mouse_point {mouse_x, mouse_y};
            SDL_Rect Transform_rect {318,837,76,15}, HSV_rect {396,837,53,15}, Blur_rect {454,837,36,15},
                Extra_rect {494,837,44,15}, NoEffects_rect {542, 837, 84,15};

            if (SDL_PointInRect(&mouse_point, &Transform_rect))
                Rend.DrawRectangle({0,0,255}, Transform_rect);
            else if (SDL_PointInRect(&mouse_point, &HSV_rect))
                Rend.DrawRectangle({0,0,255}, HSV_rect);
            else if (SDL_PointInRect(&mouse_point, &Blur_rect))
                Rend.DrawRectangle({0,0,255}, Blur_rect);
            else if (SDL_PointInRect(&mouse_point, &Extra_rect))
                Rend.DrawRectangle({0,0,255}, Extra_rect);
            else if (SDL_PointInRect(&mouse_point, &NoEffects_rect))
                Rend.DrawRectangle({0,0,255}, NoEffects_rect);

            if (clicked_on.has_value()) {

                if (SDL_PointInRect(&clicked_on.value(), &Transform_rect)) {
                   currentShader = &distortionShader;
                   Rend.FillRectangle({0,0,255}, Transform_rect);
                }
                else if (SDL_PointInRect(&clicked_on.value(), &HSV_rect)) {
                   currentShader = &hsvHueRotationShader;
                   Rend.FillRectangle({0,0,255}, HSV_rect);
                }
                else if (SDL_PointInRect(&clicked_on.value(), &Blur_rect)) {
                   currentShader = &blurShader;
                   Rend.FillRectangle({0,0,255}, Blur_rect);
                }

                else if (SDL_PointInRect(&clicked_on.value(), &Extra_rect)) {
                   currentShader = &testShader;
                   Rend.FillRectangle({0,0,255}, Extra_rect);
                }
                else {
                   Rend.OutOfBoundsType = dss::Renderer::OutOfBoundsType::Repeat;
                   currentShader = nullptr;
                   Rend.FillRectangle({0,0,255}, NoEffects_rect);
                }
            }

            auto l = [](const dss::Renderer& renderer[[maybe_unused]], int x, int y) -> dss::RGBA {
                return {
                        static_cast<uint8_t>(x - WINDOW_WIDTH/4-32),
                        static_cast<uint8_t>(y+5),
                        static_cast<uint8_t>(255 - (x - WINDOW_WIDTH/4-32))};
            };
            Rend.FillRectangle(l, SDL_Rect{WINDOW_WIDTH/4-32, 158, 64, 64});

            Rend.DrawLine(0x0000ffff, WINDOW_WIDTH/2+100, 200, WINDOW_WIDTH-50, 10, true, true);
            Rend.DrawLine(0x0000ffff, WINDOW_WIDTH/2+100, 210,    WINDOW_WIDTH-50, 20);
            Rend.DrawLine(0x0000ffff, WINDOW_WIDTH/2+100, 220, WINDOW_WIDTH-50, 30, true);

            Rend.FillCircle(0x0000ffff, WINDOW_WIDTH/2, 64, 50, true);
            Rend.FillCircle(0x0000ffff, WINDOW_WIDTH/2, 190, 50, false);

            Rend.FillTriangle(
                dss::RGB::Red,
                dss::RGB::Green,
                dss::RGB::Blue,
                WINDOW_WIDTH/4-32, 		32,
                WINDOW_WIDTH/4-32, 		32 + 64,
                WINDOW_WIDTH/4, 	32 + 32);
            Rend.FillTriangle(
                dss::RGB::Green,
                dss::RGB::Blue,
                dss::RGB::Red,
                WINDOW_WIDTH/4+32, 	32,
                WINDOW_WIDTH/4, 	32 + 32,
                WINDOW_WIDTH/4+32, 	32 + 64);
            Rend.FillTriangle(
                dss::RGB::Green,
                dss::RGB::Blue,
                dss::RGB::Red,
                WINDOW_WIDTH/4-32, 		32 + 64,
                WINDOW_WIDTH/4, 	32 + 32,
                WINDOW_WIDTH/4+32, 	32 + 64);
            Rend.FillTriangle(
                dss::RGB::Red,
                dss::RGB::Green,
                dss::RGB::Blue,
                WINDOW_WIDTH/4-32, 		32,
                WINDOW_WIDTH/4+32, 	32,
                WINDOW_WIDTH/4, 	32 + 32);


            SDL_Point p1{WINDOW_WIDTH/2-250, 300};
            SDL_Point p2{WINDOW_WIDTH/2-250, 800};
            SDL_Point p3{WINDOW_WIDTH/2+250, 800};
            SDL_Point p4{WINDOW_WIDTH/2+250, 300};

            SDL_Point pInBetween {(p1.x + p2.x + p3.x + p4.x) / 4, (p1.y + p2.y + p3.y + p4.y) / 4};

            Rend.DrawTriangle(triangleTexture,
                                  0 + val2/100.f, 0 - val1/100.f,
                                  0 + val2/100.f, 1 - val1/100.f,
                                  1 + val2/100.f, 1 - val1/100.f,
                                  p1.x, p1.y,
                                  p2.x, p2.y,
                                  p3.x, p3.y);
            Rend.DrawTriangle(triangleTexture,
                                  0 + val2/100.f, 0 - val1/100.f,
                                  1 + val2/100.f, 1 - val1/100.f,
                                  1 + val2/100.f, 0 - val1/100.f,
                                  p1.x, p1.y,
                                  p3.x, p3.y,
                                  p4.x, p4.y);


            auto RandomColor = [seed = 43566333]  (int min = 0, int max = 255) mutable -> dss::RGBA {
                seed += 3489;
                return dss::RGBA{(uint8_t)std::clamp(seed*2378%255, min, max), (uint8_t)std::clamp(seed*899%255, min, max), (uint8_t)std::clamp(seed*328%255,min,max)};
            };


            int wordCount = 0;
            for(std::string_view word : { "Transform", "HSVHue", "Blur", "Extra", "No effects"}) {
                Rend.DrawText(word, 320 + wordCount * dss::Fonts::Raster8x16.GetFontWidth(), 820 + dss::Fonts::Raster8x16.GetFontHeight(),
                                  {dss::Fonts::Raster8x16, false, dss::TextAlignment::TL, 1, RandomColor(128, 255), 0, dss::RGBA::Black, 1, 1});
                wordCount += word.length();
                wordCount++;
            }


            if(currentShader != nullptr) {
                if(currentShader == &blurShader) {
                    Rend.FillRectangle(&hBlurShader, mouse_x-shaderRectSize/2, mouse_y-shaderRectSize/2, shaderRectSize, shaderRectSize);
                    Rend.FillRectangle(&vBlurShader, mouse_x-shaderRectSize/2, mouse_y-shaderRectSize/2, shaderRectSize, shaderRectSize);
                }
                else {
                    Rend.FillRectangle(currentShader, mouse_x-shaderRectSize/2, mouse_y-shaderRectSize/2, shaderRectSize, shaderRectSize);
                }
                Rend.DrawRectangle({0xe0, 0xef, 0xff}, mouse_x-shaderRectSize/2, mouse_y-shaderRectSize/2, shaderRectSize, shaderRectSize);
            }


            SDL_UpdateTexture(texture, nullptr, pixels.data(), C_WIDTH * sizeof(uint32_t));
            SDL_RenderClear(renderer);
            SDL_Rect rect1{0,0, C_WIDTH, C_HEIGHT};
            SDL_Rect rect2{0,0, C_WIDTH, WINDOW_HEIGHT};
            SDL_RenderCopy(renderer, texture, &rect1, &rect2);
            SDL_RenderPresent(renderer);
        }

        SDL_Quit();

    return 0;
}


