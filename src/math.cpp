#include "math.h"

math::vec2::vec2() : x{0}, y{0} { }
math::vec2::vec2(float x, float y) : x{x}, y{y} { }

void math::vec2::setLength(float value) {
    float ratio = value / dist();
    x = ratio * x;
    y = ratio * y;
}
void math::vec2::addLength(float value) {
    setLength(dist() + value);
}
void math::vec2::subLength(float value) {
    setLength(dist() - value);
}
math::vec2& math::vec2::operator=(const math::vec2& other) {
    this->x = other.x;
    this->y = other.y;
    return *this;
}
math::vec2& math::vec2::operator+=(const math::vec2& other) {
    this->x += other.x;
    this->y += other.y;
    return *this;
}
math::vec2& math::vec2::operator-=(const math::vec2& other) {
    this->x -= other.x;
    this->y -= other.y;
    return *this;
}
float math::vec2::operator^=(const vec2& other) {
    float crossValue = this->x * other.y - other.x * this->y;
    return crossValue;
}
math::vec2& math::vec2::operator/=(const float other) {
    this->x /= other;
    this->y /= other;
    return *this;
}
math::vec2 math::vec2::operator+(const math::vec2& other) const {
    return math::vec2 { this->x + other.x, this->y + other.y };
}
math::vec2 math::vec2::operator-(const math::vec2& other) const {
    return math::vec2 { this->x - other.x, this->y - other.y };
}
float math::vec2::operator*(const math::vec2& other) const {
    return this->x * other.x + this->y * other.y;
}
math::vec2 math::vec2::operator*(const float other) const {
    return math::vec2 { this->x * other, this->y * other };
}
float math::vec2::operator^(const math::vec2& other) const {
    return this->x * other.x + this->y * other.y;
}
math::vec2 math::vec2::operator/(const float other) const {
    return math::vec2 { this->x / other, this->y / other };
}

math::vec3::vec3() : x{0}, y{0}, z{0} { }
math::vec3::vec3(float x, float y, float z) : x{x}, y{y}, z{z} { }

void math::vec3::setLength(float value) {
    float ratio = value / dist();
    x = ratio * x;
    y = ratio * y;
    z = ratio * z;
}
void math::vec3::addLength(float value) {
    setLength(dist() + value);
}
void math::vec3::subLength(float value) {
    setLength(dist() - value);
}
math::vec3& math::vec3::operator=(const math::vec3& other) {
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    return *this;
}
math::vec3& math::vec3::operator+=(const math::vec3& other) {
    this->x += other.x;
    this->y += other.y;
    this->z += other.z;
    return *this;
}
math::vec3& math::vec3::operator-=(const math::vec3& other) {
    this->x -= other.x;
    this->y -= other.y;
    this->z -= other.z;
    return *this;
}
math::vec3& math::vec3::operator*=(const float other) {
    this->x *= other;
    this->y *= other;
    this->z *= other;
    return *this;
}
math::vec3& math::vec3::operator^=(const math::vec3& other) {
    *this = *this ^ other;
    return *this;
}
math::vec3& math::vec3::operator/=(const float other) {
    this->x /= other;
    this->y /= other;
    this->z /= other;
    return *this;
}
math::vec3 math::vec3::operator+(const math::vec3& other) const {
    return math::vec3 { this->x + other.x, this->y + other.y, this->z + other.z };
}
math::vec3 math::vec3::operator-(const math::vec3& other) const {
    return math::vec3 { this->x - other.x, this->y - other.y, this->z - other.z };
}
math::vec3 math::vec3::operator*(const float other) const {
    return math::vec3 { this->x * other, this->y * other, this->z * other };
}
float math::vec3::operator*(const math::vec3& other) const {
    return this->x * other.x + this->y * other.y + this->z * other.z;
}
math::vec3 math::vec3::operator^(const math::vec3& other) const {
    return { this->y * other.z - this->z * other.y, this->z * other.x - this->x * other.z, this->x * other.y - this->y * other.x };
}
math::vec3 math::vec3::operator/(const float other) const {
    return math::vec3 { this->x / other, this->y / other, this->z / other };
}

math::vec4::vec4() : x{0}, y{0}, z{0}, w{1} { }
math::vec4::vec4(float x, float y, float z, float w) : x{x}, y{y}, z{z}, w{w} { }

void math::vec4::setLength(float value) {
    float ratio = value / dist();
    x = ratio * x;
    y = ratio * y;
    z = ratio * z;
    w = ratio * w;
}
void math::vec4::addLength(float value) {
    setLength(dist() + value);
}
void math::vec4::subLength(float value) {
    setLength(dist() - value);
}
math::vec4& math::vec4::operator=(const math::vec4& other) {
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    this->w = other.w;
    return *this;
}
math::vec4& math::vec4::operator+=(const math::vec4& other) {
    this->x += other.x;
    this->y += other.y;
    this->z += other.z;
    return *this;
}
math::vec4& math::vec4::operator-=(const math::vec4& other) {
    this->x -= other.x;
    this->y -= other.y;
    this->z -= other.z;
    return *this;
}
math::vec4& math::vec4::operator*=(const float other) {
    this->x *= other;
    this->y *= other;
    this->z *= other;
    return *this;
}
math::vec4& math::vec4::operator^=(const math::vec4& other) {
    *this = *this ^ other;
    return *this;
}
math::vec4& math::vec4::operator/=(const float other) {
    this->x /= other;
    this->y /= other;
    this->z /= other;
    return *this;
}
math::vec4 math::vec4::operator+(const math::vec4& other) const {
    return math::vec4 { this->x + other.x, this->y + other.y, this->z + other.z };
}
math::vec4 math::vec4::operator-(const math::vec4& other) const {
    return math::vec4 { this->x - other.x, this->y - other.y, this->z - other.z };
}
float math::vec4::operator*(const math::vec4& other) const {
    return this->x * other.x + this->y * other.y + this->z * other.z;
}
math::vec4 math::vec4::operator*(const float other) const {
    return math::vec4 { this->x * other, this->y * other, this->z * other };
}
math::vec4 math::vec4::operator^(const math::vec4& other) const {
    return math::vec4 { this->y * other.z - this->z * other.y, this->z * other.x - this->x * other.z, this->x * other.y - this->y * other.x, 1 };
}
math::vec4 math::vec4::operator/(const float other) const {
    return math::vec4 { this->x / other, this->y / other, this->z / other };
}

///////////////////////////

math::ivec2::ivec2() : x{0}, y{0} { }
math::ivec2::ivec2(int x, int y) : x{x}, y{y} { }

void math::ivec2::setLength(int value) {
    float ratio = value / dist();
    x = ratio * x;
    y = ratio * y;
}
void math::ivec2::addLength(int value) {
    setLength(dist() + value);
}
void math::ivec2::subLength(int value) {
    setLength(dist() - value);
}
math::ivec2& math::ivec2::operator=(const math::ivec2& other) {
    this->x = other.x;
    this->y = other.y;
    return *this;
}
math::ivec2& math::ivec2::operator+=(const math::ivec2& other) {
    this->x += other.x;
    this->y += other.y;
    return *this;
}
math::ivec2& math::ivec2::operator-=(const math::ivec2& other) {
    this->x -= other.x;
    this->y -= other.y;
    return *this;
}
int math::ivec2::operator^=(const ivec2& other) {
    int crossValue = this->x * other.y - other.x * this->y;
    return crossValue;
}
math::ivec2& math::ivec2::operator/=(const int other) {
    this->x /= other;
    this->y /= other;
    return *this;
}
math::ivec2 math::ivec2::operator+(const math::ivec2& other) {
    return math::ivec2 { this->x + other.x, this->y + other.y };
}
math::ivec2 math::ivec2::operator-(const math::ivec2& other) {
    return math::ivec2 { this->x - other.x, this->y - other.y };
}
int math::ivec2::operator*(const math::ivec2& other) {
    return this->x * other.x + this->y * other.y;
}
math::ivec2 math::ivec2::operator*(const int other) {
    return math::ivec2 { this->x * other, this->y * other };
}
int math::ivec2::operator^(const math::ivec2& other) {
    return this->x * other.x + this->y * other.y;
}
math::ivec2 math::ivec2::operator/(const int other) {
    return math::ivec2 { this->x / other, this->y / other };
}

math::ivec3::ivec3() : x{0}, y{0}, z{0} { }
math::ivec3::ivec3(int x, int y, int z) : x{x}, y{y}, z{z} { }

void math::ivec3::setLength(int value) {
    float ratio = value / dist();
    x = ratio * x;
    y = ratio * y;
    z = ratio * z;
}
void math::ivec3::addLength(int value) {
    setLength(dist() + value);
}
void math::ivec3::subLength(int value) {
    setLength(dist() - value);
}
math::ivec3& math::ivec3::operator=(const math::ivec3& other) {
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    return *this;
}
math::ivec3& math::ivec3::operator+=(const math::ivec3& other) {
    this->x += other.x;
    this->y += other.y;
    this->z += other.z;
    return *this;
}
math::ivec3& math::ivec3::operator-=(const math::ivec3& other) {
    this->x -= other.x;
    this->y -= other.y;
    this->z -= other.z;
    return *this;
}
math::ivec3& math::ivec3::operator*=(const int other) {
    this->x *= other;
    this->y *= other;
    this->z *= other;
    return *this;
}
math::ivec3& math::ivec3::operator^=(const math::ivec3& other) {
    *this = *this ^ other;
    return *this;
}
math::ivec3& math::ivec3::operator/=(const int other) {
    this->x /= other;
    this->y /= other;
    this->z /= other;
    return *this;
}
math::ivec3 math::ivec3::operator+(const math::ivec3& other) {
    return math::ivec3 { this->x + other.x, this->y + other.y, this->z + other.z };
}
math::ivec3 math::ivec3::operator-(const math::ivec3& other) {
    return math::ivec3 { this->x - other.x, this->y - other.y, this->z - other.z };
}
math::ivec3 math::ivec3::operator*(const int other) {
    return math::ivec3 { this->x * other, this->y * other, this->z * other };
}
int math::ivec3::operator*(const math::ivec3& other) {
    return this->x * other.x + this->y * other.y + this->z * other.z;
}
math::ivec3 math::ivec3::operator^(const math::ivec3& other) {
    return { this->y * other.z - this->z * other.y, this->z * other.x - this->x * other.z, this->x * other.y - this->y * other.x };
}
math::ivec3 math::ivec3::operator/(const int other) {
    return math::ivec3 { this->x / other, this->y / other, this->z / other };
}

math::ivec4::ivec4() : x{0}, y{0}, z{0}, w{0} { }
math::ivec4::ivec4(int x, int y, int z, int w) : x{x}, y{y}, z{z}, w{w} { }

void math::ivec4::setLength(int value) {
    float ratio = value / dist();
    x = ratio * x;
    y = ratio * y;
    z = ratio * z;
    w = ratio * w;
}
void math::ivec4::addLength(int value) {
    setLength(dist() + value);
}
void math::ivec4::subLength(int value) {
    setLength(dist() - value);
}
math::ivec4& math::ivec4::operator=(const math::ivec4& other) {
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    this->w = other.w;
    return *this;
}
math::ivec4& math::ivec4::operator+=(const math::ivec4& other) {
    this->x += other.x;
    this->y += other.y;
    this->z += other.z;
    this->w += other.w;
    return *this;
}
math::ivec4& math::ivec4::operator-=(const math::ivec4& other) {
    this->x -= other.x;
    this->y -= other.y;
    this->z -= other.z;
    this->w -= other.w;
    return *this;
}
math::ivec4& math::ivec4::operator*=(const int other) {
    this->x *= other;
    this->y *= other;
    this->z *= other;
    this->w *= other;
    return *this;
}
math::ivec4& math::ivec4::operator/=(const int other) {
    this->x /= other;
    this->y /= other;
    this->z /= other;
    this->w /= other;
    return *this;
}
math::ivec4 math::ivec4::operator+(const math::ivec4& other) {
    return math::ivec4 { this->x + other.x, this->y + other.y, this->z + other.z, this->w + other.w };
}
math::ivec4 math::ivec4::operator-(const math::ivec4& other) {
    return math::ivec4 { this->x - other.x, this->y - other.y, this->z - other.z, this->w - other.w };
}
int math::ivec4::operator*(const math::ivec4& other) {
    return this->x * other.x + this->y * other.y + this->z * other.z + this->w * other.w;
}
math::ivec4 math::ivec4::operator*(const int other) {
    return math::ivec4 { this->x * other, this->y * other, this->z * other, this->w * other };
}
math::ivec4 math::ivec4::operator/(const int other) {
    return math::ivec4 { this->x / other, this->y / other, this->z / other, this->w / other };
}

//////////////////////

math::mat2x2::mat2x2() : mat{ } {
}
math::mat2x2::mat2x2(const float initValue) : mat{ }{
    for(int i = 0; i < 2; i++)
        this->mat[i+2*i] = initValue;
}
math::mat2x2::mat2x2(const float mat[4]) {
    for(int i = 0; i < 4; i++)
        this->mat[i] = mat[i];
}
math::mat2x2::mat2x2(const math::mat2x2& other) {
    for(int i = 0; i < 4; i++)
        this->mat[i] = other.mat[i];
}
math::mat2x2::mat2x2(std::initializer_list<float> list) {
    int counter{0};
    for (auto element : list) {
        mat[counter] = element;
        ++counter;
    }
}
math::mat2x2 math::mat2x2::operator=(std::initializer_list<float> list) {
    int counter{0};
    for (auto element : list) {
        mat[counter] = element;
        ++counter;
    }
    return *this;
}

math::mat2x2 math::mat2x2::transpose() {
    mat2x2 mat;

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            mat.mat[i + j * 2] = this->mat[j + i * 2];
        }
    }

    return mat;
}
math::mat2x2 math::mat2x2::inverse() {
    mat2x2 mat = mat2x2::Null();

    float det = this->determinant();
    if(det == 0) return mat;

    float invdet = 1.f / det;

    mat.mat[0] =  this->mat[3] * invdet;
    mat.mat[1] = -this->mat[1] * invdet;
    mat.mat[2] = -this->mat[2] * invdet;
    mat.mat[3] =  this->mat[0] * invdet;

    return mat;
}
math::mat2x2::row math::mat2x2::operator[](int index) {
    return math::mat2x2::row{this->mat, index};
}
const math::mat2x2::row math::mat2x2::operator[](int index) const {
    return math::mat2x2::row{this->mat, index};
}
math::mat2x2& math::mat2x2::operator=(const math::mat2x2& other) {
    for(int i = 0; i < 4; i++)
        this->mat[i] = other.mat[i];
    return *this;
}
math::mat2x2& math::mat2x2::operator+=(const math::mat2x2& other) {
    for(int i = 0; i < 4; i++) {
        this->mat[i] += other.mat[i];
    }
    return *this;
}
math::mat2x2& math::mat2x2::operator-=(const math::mat2x2& other) {
    for(int i = 0; i < 4; i++) {
        this->mat[i] -= other.mat[i];
    }
    return *this;
}
math::mat2x2& math::mat2x2::operator*=(const math::mat2x2& other) {
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 2; j++) {
            this->mat[i + j * 2] =
                this->mat[j * 2] * other.mat[j + i] +
                this->mat[j * 2 + 1] * other.mat[j * 2 + i];
        }
    }

    return *this;
}
math::mat2x2& math::mat2x2::operator*=(const float other) {
    for(int i = 0; i < 4; i++) {
        this->mat[i] *= other;
    }

    return *this;
}
math::mat2x2& math::mat2x2::operator/=(const float other) {
    for(int i = 0; i < 4; i++) {
        this->mat[i] /= other;
    }

    return *this;
}
math::mat2x2 math::mat2x2::operator+(const math::mat2x2& other) {
    math::mat2x2 temp;
    for(int i = 0; i < 4; i++) {
        temp.mat[i] = this->mat[i] + other.mat[i];
    }
    return temp;
}
math::mat2x2 math::mat2x2::operator-(const math::mat2x2& other) {
    math::mat2x2 temp;
    for(int i = 0; i < 4; i++) {
        temp.mat[i] = this->mat[i] - other.mat[i];
    }
    return temp;
}
math::mat2x2 math::mat2x2::operator*(const math::mat2x2& other) {
    math::mat2x2 temp;

    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 2; j++) {
            temp.mat[i + j * 2] =
                this->mat[j * 2] * other.mat[i] +
                this->mat[j * 2 + 1] * other.mat[2 + i];
        }
    }

    return temp;
}
math::vec2 math::mat2x2::operator*(const math::vec2& other) {
    return math::vec2 {
        other.x * this->mat[0] + other.y * this->mat[1],
        other.x * this->mat[2] + other.y * this->mat[3]
    };
}
math::mat2x2 math::mat2x2::operator*(const float other) {
    math::mat2x2 temp = math::mat2x2(*this);

    for(int i = 0; i < 4; i++) {
        temp.mat[i] *= other;
    }

    return temp;
}
math::mat2x2 math::mat2x2::operator/(const float other) {
    math::mat2x2 temp = math::mat2x2(*this);

    for(int i = 0; i < 4; i++) {
        temp.mat[i] /= other;
    }

    return temp;
}

math::mat3x3::mat3x3() : mat{ } {
}
math::mat3x3::mat3x3(const float initValue) : mat{ }{
    for(int i = 0; i < 3; i++)
        this->mat[i+3*i] = initValue;
}
math::mat3x3::mat3x3(const float mat[9]) {
    for(int i = 0; i < 9; i++)
        this->mat[i] = mat[i];
}
math::mat3x3::mat3x3(const math::mat3x3& other) {
    for(int i = 0; i < 9; i++)
        this->mat[i] = other.mat[i];
}
math::mat3x3::mat3x3(std::initializer_list<float> list) {
    int counter{0};
    for (auto element : list) {
        mat[counter] = element;
        ++counter;
    }
}
math::mat3x3 math::mat3x3::operator=(std::initializer_list<float> list) {
    int counter{0};
    for (auto element : list) {
        mat[counter] = element;
        ++counter;
    }
    return *this;
}

void math::mat3x3::translate(float x, float y) {
    this->mat[2] = x;
    this->mat[5] = y;
}
void math::mat3x3::scale(float sx, float sy) {
    this->mat[0] = sx;
    this->mat[4] = sy;
}
void math::mat3x3::rotate(float angle) {
    this->mat[0] = cosf(angle);
    this->mat[1] = -sinf(angle);
    this->mat[3] = sinf(angle);
    this->mat[4] = cosf(angle);
}
math::mat3x3 math::mat3x3::transpose() {
    mat3x3 mat;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            mat.mat[i + j * 3] = this->mat[j + i * 3];
        }
    }

    return mat;
}
math::mat3x3 math::mat3x3::inverse() {
    mat3x3 mat = mat3x3::Null();

    float det = this->determinant();
    if(det == 0) return mat;

    float invdet = 1.f / det;

    mat.mat[0 + 0 * 3] = (this->mat[1 + 1 * 3] * this->mat[2 + 2 * 3] - this->mat[2 + 1 * 3] * this->mat[1 + 2 * 3]) * invdet;
    mat.mat[0 + 1 * 3] = (this->mat[0 + 2 * 3] * this->mat[2 + 1 * 3] - this->mat[0 + 1 * 3] * this->mat[2 + 2 * 3]) * invdet;
    mat.mat[0 + 2 * 3] = (this->mat[0 + 1 * 3] * this->mat[1 + 2 * 3] - this->mat[0 + 2 * 3] * this->mat[1 + 1 * 3]) * invdet;
    mat.mat[1 + 0 * 3] = (this->mat[1 + 2 * 3] * this->mat[2 + 0 * 3] - this->mat[1 + 0 * 3] * this->mat[2 + 2 * 3]) * invdet;
    mat.mat[1 + 1 * 3] = (this->mat[0 + 0 * 3] * this->mat[2 + 2 * 3] - this->mat[0 + 2 * 3] * this->mat[2 + 0 * 3]) * invdet;
    mat.mat[1 + 2 * 3] = (this->mat[1 + 0 * 3] * this->mat[0 + 2 * 3] - this->mat[0 + 0 * 3] * this->mat[1 + 2 * 3]) * invdet;
    mat.mat[2 + 0 * 3] = (this->mat[1 + 0 * 3] * this->mat[2 + 1 * 3] - this->mat[2 + 0 * 3] * this->mat[1 + 1 * 3]) * invdet;
    mat.mat[2 + 1 * 3] = (this->mat[2 + 0 * 3] * this->mat[0 + 1 * 3] - this->mat[0 + 0 * 3] * this->mat[2 + 1 * 3]) * invdet;
    mat.mat[2 + 2 * 3] = (this->mat[0 + 0 * 3] * this->mat[1 + 1 * 3] - this->mat[1 + 0 * 3] * this->mat[0 + 1 * 3]) * invdet;


    return mat;
}
math::mat3x3::row math::mat3x3::operator[](int index) {
    return math::mat3x3::row{this->mat, index};
}
const math::mat3x3::row math::mat3x3::operator[](int index) const {
    return math::mat3x3::row{this->mat, index};
}
math::mat3x3& math::mat3x3::operator=(const math::mat3x3& other) {
    for(int i = 0; i < 9; i++)
        this->mat[i] = other.mat[i];
    return *this;
}
math::mat3x3& math::mat3x3::operator+=(const math::mat3x3& other) {
    for(int i = 0; i < 9; i++) {
        this->mat[i] += other.mat[i];
    }
    return *this;
}
math::mat3x3& math::mat3x3::operator-=(const math::mat3x3& other) {
    for(int i = 0; i < 9; i++) {
        this->mat[i] -= other.mat[i];
    }
    return *this;
}
math::mat3x3& math::mat3x3::operator*=(const math::mat3x3& other) {
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            this->mat[i + j * 3] =
                this->mat[j * 3] * other.mat[i] +
                this->mat[j * 3 + 1] * other.mat[3 + i] +
                this->mat[j * 3 + 2] * other.mat[6 + i];
        }
    }
    return *this;
}
math::mat3x3& math::mat3x3::operator*=(const float other) {
    for(int i = 0; i < 9; i++) {
        this->mat[i] *= other;
    }

    return *this;
}
math::mat3x3& math::mat3x3::operator/=(const float other) {
    for(int i = 0; i < 9; i++) {
        this->mat[i] /= other;
    }

    return *this;
}
math::mat3x3 math::mat3x3::operator+(const math::mat3x3& other) {
    math::mat3x3 temp;
    for(int i = 0; i < 9; i++) {
        temp.mat[i] = this->mat[i] + other.mat[i];
    }
    return temp;
}
math::mat3x3 math::mat3x3::operator-(const math::mat3x3& other) {
    math::mat3x3 temp;
    for(int i = 0; i < 9; i++) {
        temp.mat[i] = this->mat[i] - other.mat[i];
    }
    return temp;
}
math::mat3x3 math::mat3x3::operator*(const math::mat3x3& other) {
    math::mat3x3 temp;

    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            temp.mat[i + j * 3] =
                this->mat[j * 3] * other.mat[i] +
                this->mat[j * 3 + 1] * other.mat[3 + i] +
                this->mat[j * 3 + 2] * other.mat[6 + i];
        }
    }

    return temp;
}
math::vec3 math::mat3x3::operator*(const math::vec3& other) {
    return math::vec3 {
        other.x * this->mat[0] + other.y * this->mat[1] + other.z * this->mat[2],
        other.x * this->mat[3] + other.y * this->mat[4] + other.z * this->mat[5],
        other.x * this->mat[6] + other.y * this->mat[7] + other.z * this->mat[8]
    };
}
math::mat3x3 math::mat3x3::operator*(const float other) {
    math::mat3x3 temp = math::mat3x3(*this);

    for(int i = 0; i < 4; i++) {
        temp.mat[i] *= other;
    }

    return temp;
}
math::mat3x3 math::mat3x3::operator/(const float other) {
    math::mat3x3 temp = math::mat3x3(*this);

    for(int i = 0; i < 4; i++) {
        temp.mat[i] /= other;
    }

    return temp;
}

math::mat4x4::mat4x4() : mat{ }{
}
math::mat4x4::mat4x4(const float initValue) : mat{ } {
    for(int i = 0; i < 4; i++) {
        this->mat[i+4*i] = initValue;
    }
}
math::mat4x4::mat4x4(const float mat[16]) {
    for(int i = 0; i < 16; i++)
        this->mat[i] = mat[i];
}
math::mat4x4::mat4x4(const math::mat4x4& other) {
    for(int i = 0; i < 16; i++)
        this->mat[i] = other.mat[i];
}
math::mat4x4::mat4x4(std::initializer_list<float> list) {
    int counter{0};
    for (auto element : list) {
        mat[counter] = element;
        ++counter;
    }
}
math::mat4x4 math::mat4x4::operator=(std::initializer_list<float> list) {
    int counter{0};
    for (auto element : list) {
        mat[counter] = element;
        ++counter;
    }
    return *this;
}

void math::mat4x4::translate(float x, float y, float z) {
    this->mat[3] = x;
    this->mat[7] = y;
    this->mat[11] = z;
}
void math::mat4x4::scale(float sx, float sy, float sz) {
    this->mat[0] = sx;
    this->mat[5] = sy;
    this->mat[10] = sz;
}
void math::mat4x4::rotate(float angle, vec3 rotationAxis) {
    float cosAngle = cosf(angle);
    float sinAngle = sinf(angle);
    this->mat[0] = cosAngle + rotationAxis.x * rotationAxis.x * (1 - cosAngle);
    this->mat[1] = rotationAxis.x * rotationAxis.y * (1 - cosAngle) - rotationAxis.z * sinAngle;
    this->mat[2] = rotationAxis.x * rotationAxis.z * (1 - cosAngle) + rotationAxis.y * sinAngle;
    this->mat[4] = rotationAxis.y * rotationAxis.x * (1 - cosAngle) + rotationAxis.z * sinAngle;
    this->mat[5] = cosAngle + rotationAxis.y * rotationAxis.y * (1 - cosAngle);
    this->mat[6] = rotationAxis.y * rotationAxis.z * (1 - cosAngle) - rotationAxis.x * sinAngle;
    this->mat[8] = rotationAxis.z * rotationAxis.x * (1 - cosAngle) - rotationAxis.y * sinAngle;
    this->mat[9] = rotationAxis.z * rotationAxis.y * (1 - cosAngle) + rotationAxis.x * sinAngle;
    this->mat[10] = cosAngle + rotationAxis.z * rotationAxis.z * (1 - cosAngle);
}
math::mat4x4 math::mat4x4::transpose() {
    mat4x4 mat;

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            mat.mat[i + j * 4] = this->mat[j + i * 4];
        }
    }

    return mat;
}
math::mat4x4 math::mat4x4::inverse() {
    mat4x4 mat = mat4x4::Null();

    float det = this->determinant();
    if(det == 0) return mat;

    float invdet = 1.f / det;

    mat.mat[0] = (this->mat[5]  * this->mat[10] * this->mat[15] -
                  this->mat[5]  * this->mat[11] * this->mat[14] -
                  this->mat[9]  * this->mat[6]  * this->mat[15] +
                  this->mat[9]  * this->mat[7]  * this->mat[14] +
                  this->mat[13] * this->mat[6]  * this->mat[11] -
                  this->mat[13] * this->mat[7]  * this->mat[10]) * invdet;

    mat.mat[4] = (-this->mat[4]  * this->mat[10] * this->mat[15] +
                  this->mat[4]  * this->mat[11] * this->mat[14] +
                  this->mat[8]  * this->mat[6]  * this->mat[15] -
                  this->mat[8]  * this->mat[7]  * this->mat[14] -
                  this->mat[12] * this->mat[6]  * this->mat[11] +
                  this->mat[12] * this->mat[7]  * this->mat[10]) * invdet;

    mat.mat[8] = (this->mat[4]  * this->mat[9] * this->mat[15] -
                  this->mat[4]  * this->mat[11] * this->mat[13] -
                  this->mat[8]  * this->mat[5] * this->mat[15] +
                  this->mat[8]  * this->mat[7] * this->mat[13] +
                  this->mat[12] * this->mat[5] * this->mat[11] -
                  this->mat[12] * this->mat[7] * this->mat[9]) * invdet;

    mat.mat[12] = (-this->mat[4]  * this->mat[9] * this->mat[14] +
                   this->mat[4]  * this->mat[10] * this->mat[13] +
                   this->mat[8]  * this->mat[5] * this->mat[14] -
                   this->mat[8]  * this->mat[6] * this->mat[13] -
                   this->mat[12] * this->mat[5] * this->mat[10] +
                   this->mat[12] * this->mat[6] * this->mat[9]) * invdet;

    mat.mat[1] = (-this->mat[1]  * this->mat[10] * this->mat[15] +
                  this->mat[1]  * this->mat[11] * this->mat[14] +
                  this->mat[9]  * this->mat[2] * this->mat[15] -
                  this->mat[9]  * this->mat[3] * this->mat[14] -
                  this->mat[13] * this->mat[2] * this->mat[11] +
                  this->mat[13] * this->mat[3] * this->mat[10]) * invdet;

    mat.mat[5] = (this->mat[0]  * this->mat[10] * this->mat[15] -
                  this->mat[0]  * this->mat[11] * this->mat[14] -
                  this->mat[8]  * this->mat[2] * this->mat[15] +
                  this->mat[8]  * this->mat[3] * this->mat[14] +
                  this->mat[12] * this->mat[2] * this->mat[11] -
                  this->mat[12] * this->mat[3] * this->mat[10]) * invdet;

    mat.mat[9] = (-this->mat[0]  * this->mat[9] * this->mat[15] +
                  this->mat[0]  * this->mat[11] * this->mat[13] +
                  this->mat[8]  * this->mat[1] * this->mat[15] -
                  this->mat[8]  * this->mat[3] * this->mat[13] -
                  this->mat[12] * this->mat[1] * this->mat[11] +
                  this->mat[12] * this->mat[3] * this->mat[9]) * invdet;

    mat.mat[13] = (this->mat[0]  * this->mat[9] * this->mat[14] -
                   this->mat[0]  * this->mat[10] * this->mat[13] -
                   this->mat[8]  * this->mat[1] * this->mat[14] +
                   this->mat[8]  * this->mat[2] * this->mat[13] +
                   this->mat[12] * this->mat[1] * this->mat[10] -
                   this->mat[12] * this->mat[2] * this->mat[9]) * invdet;

    mat.mat[2] = (this->mat[1]  * this->mat[6] * this->mat[15] -
                  this->mat[1]  * this->mat[7] * this->mat[14] -
                  this->mat[5]  * this->mat[2] * this->mat[15] +
                  this->mat[5]  * this->mat[3] * this->mat[14] +
                  this->mat[13] * this->mat[2] * this->mat[7] -
                  this->mat[13] * this->mat[3] * this->mat[6]) * invdet;

    mat.mat[6] = (-this->mat[0]  * this->mat[6] * this->mat[15] +
                  this->mat[0]  * this->mat[7] * this->mat[14] +
                  this->mat[4]  * this->mat[2] * this->mat[15] -
                  this->mat[4]  * this->mat[3] * this->mat[14] -
                  this->mat[12] * this->mat[2] * this->mat[7] +
                  this->mat[12] * this->mat[3] * this->mat[6]) * invdet;

    mat.mat[10] = (this->mat[0]  * this->mat[5] * this->mat[15] -
                   this->mat[0]  * this->mat[7] * this->mat[13] -
                   this->mat[4]  * this->mat[1] * this->mat[15] +
                   this->mat[4]  * this->mat[3] * this->mat[13] +
                   this->mat[12] * this->mat[1] * this->mat[7] -
                   this->mat[12] * this->mat[3] * this->mat[5]) * invdet;

    mat.mat[14] = (-this->mat[0]  * this->mat[5] * this->mat[14] +
                   this->mat[0]  * this->mat[6] * this->mat[13] +
                   this->mat[4]  * this->mat[1] * this->mat[14] -
                   this->mat[4]  * this->mat[2] * this->mat[13] -
                   this->mat[12] * this->mat[1] * this->mat[6] +
                   this->mat[12] * this->mat[2] * this->mat[5]) * invdet;

    mat.mat[3] = (-this->mat[1] * this->mat[6] * this->mat[11] +
                  this->mat[1] * this->mat[7] * this->mat[10] +
                  this->mat[5] * this->mat[2] * this->mat[11] -
                  this->mat[5] * this->mat[3] * this->mat[10] -
                  this->mat[9] * this->mat[2] * this->mat[7] +
                  this->mat[9] * this->mat[3] * this->mat[6]) * invdet;

    mat.mat[7] = (this->mat[0] * this->mat[6] * this->mat[11] -
                  this->mat[0] * this->mat[7] * this->mat[10] -
                  this->mat[4] * this->mat[2] * this->mat[11] +
                  this->mat[4] * this->mat[3] * this->mat[10] +
                  this->mat[8] * this->mat[2] * this->mat[7] -
                  this->mat[8] * this->mat[3] * this->mat[6]) * invdet;

    mat.mat[11] = (-this->mat[0] * this->mat[5] * this->mat[11] +
                   this->mat[0] * this->mat[7] * this->mat[9] +
                   this->mat[4] * this->mat[1] * this->mat[11] -
                   this->mat[4] * this->mat[3] * this->mat[9] -
                   this->mat[8] * this->mat[1] * this->mat[7] +
                   this->mat[8] * this->mat[3] * this->mat[5]) * invdet;

    mat.mat[15] = (this->mat[0] * this->mat[5] * this->mat[10] -
                   this->mat[0] * this->mat[6] * this->mat[9] -
                   this->mat[4] * this->mat[1] * this->mat[10] +
                   this->mat[4] * this->mat[2] * this->mat[9] +
                   this->mat[8] * this->mat[1] * this->mat[6] -
                   this->mat[8] * this->mat[2] * this->mat[5]) * invdet;

    return mat;
}
math::mat4x4::row math::mat4x4::operator[](int index) {
    return math::mat4x4::row{this->mat, index};
}
const math::mat4x4::row math::mat4x4::operator[](int index) const {
    return math::mat4x4::row{this->mat, index};
}
math::mat4x4& math::mat4x4::operator=(const math::mat4x4& other) {
    for(int i = 0; i < 16; i++)
        this->mat[i] = other.mat[i];
    return *this;
}
math::mat4x4& math::mat4x4::operator+=(const math::mat4x4& other) {
    for(int i = 0; i < 16; i++) {
        this->mat[i] += other.mat[i];
    }
    return *this;
}
math::mat4x4& math::mat4x4::operator-=(const math::mat4x4& other) {
    for(int i = 0; i < 16; i++) {
        this->mat[i] -= other.mat[i];
    }
    return *this;
}
math::mat4x4& math::mat4x4::operator*=(const math::mat4x4& other) {
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 4; j++) {
            this->mat[i + j * 4] =
                this->mat[j * 4] * other.mat[i] +
                this->mat[j * 4 + 1] * other.mat[4 + i] +
                this->mat[j * 4 + 2] * other.mat[8 + i] +
                this->mat[j * 4 + 3] * other.mat[12 + i];
        }
    }
    return *this;
}
math::mat4x4& math::mat4x4::operator*=(const float other) {
    for(int i = 0; i < 16; i++) {
        this->mat[i] *= other;
    }

    return *this;
}
math::mat4x4& math::mat4x4::operator/=(const float other) {
    for(int i = 0; i < 16; i++) {
        this->mat[i] /= other;
    }

    return *this;
}
math::mat4x4 math::mat4x4::operator+(const math::mat4x4& other) {
    math::mat4x4 temp;
    for(int i = 0; i < 16; i++) {
        temp.mat[i] = this->mat[i] + other.mat[i];
    }
    return temp;
}
math::mat4x4 math::mat4x4::operator-(const math::mat4x4& other) {
    math::mat4x4 temp;
    for(int i = 0; i < 16; i++) {
        temp.mat[i] = this->mat[i] - other.mat[i];
    }
    return temp;
}
math::mat4x4 math::mat4x4::operator*(const math::mat4x4& other) {
    math::mat4x4 temp;

    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 4; j++) {
            temp.mat[i + j * 4] =
                this->mat[j * 4 + 0] * other.mat[ 0 + i] +
                this->mat[j * 4 + 1] * other.mat[ 4 + i] +
                this->mat[j * 4 + 2] * other.mat[ 8 + i] +
                this->mat[j * 4 + 3] * other.mat[12 + i];
        }
    }

    return temp;
}
math::vec4 math::mat4x4::operator*(const math::vec4& other) {
    return math::vec4 {
        other.x * this->mat[0] + other.y * this->mat[1] + other.z * this->mat[2] + other.w * this->mat[3],
        other.x * this->mat[4] + other.y * this->mat[5] + other.z * this->mat[6] + other.w * this->mat[7],
        other.x * this->mat[8] + other.y * this->mat[9] + other.z * this->mat[10] + other.w * this->mat[11],
        other.x * this->mat[12] + other.y * this->mat[13] + other.z * this->mat[14] + other.w * this->mat[15],
    };
}
math::mat4x4 math::mat4x4::operator*(const float other) {
    math::mat4x4 temp = math::mat4x4(*this);

    for(int i = 0; i < 16; i++) {
        temp.mat[i] *= other;
    }

    return temp;
}
math::mat4x4 math::mat4x4::operator/(const float other) {
    math::mat4x4 temp = math::mat4x4(*this);

    for(int i = 0; i < 16; i++) {
        temp.mat[i] /= other;
    }

    return temp;
}

math::mat4x4 math::perspectiveProjection(float fov, float aspectRatio, float near, float far) {
    float fovRad = 1.f / tan(fov * 0.5 / 180 * M_PI);
    math::mat4x4 m = math::mat4x4::Identity();

    m[0][0] = aspectRatio * fovRad;
    m[1][1] = fovRad;
    m[2][2] = far / (far - near);
    m[3][2] = (-far * near) / (far - near);
    m[2][3] = 1;
    m[3][3] = 0;

    return m;
}

math::mat4x4 math::rotate(const math::mat4x4& mat, float pitch, float yaw, float roll) {
    const float cg = cos(pitch);
    const float sg = sin(pitch);
    const float cb = cos(yaw);
    const float sb = sin(yaw);
    const float ca = cos(roll);
    const float sa = sin(roll);


    math::mat4x4 p {
        1,  0,   0, 0,
        0, cg, -sg, 0,
        0, sg,  cg, 0,
        0,  0,   0, 1,
    };
    math::mat4x4 y {
        cb, 0, sb, 0,
        0, 1, 0, 0,
        -sb, 0, cb, 0,
        0, 0, 0, 1
    };
    math::mat4x4 r {
        ca, -sa, 0, 0,
        sa, ca, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };


    math::mat4x4 m = p * y * r;

    return m * mat;
}
math::mat4x4 math::rotate(const math::mat4x4& mat, float angle, math::vec3 axis) {
    axis.normalize();
    const float ca = cos(angle);
    const float sa = sin(angle);

    const float &rx = axis.x, ry = axis.y, rz = axis.z;

    mat4x4 m {
        ca+rx*rx*(1-ca), rx*ry*(1-ca)-rz*sa, rx*rz*(1-ca)+ry*sa, 0,
        ry*rx*(1-ca)+rz*sa, ca+ry*ry*(1-ca), ry*rz*(1-ca)-rx*sa, 0,
        rz*rx*(1-ca)-ry*sa, rz*ry*(1-ca)+rx*sa, ca+rz*rz*(1-ca), 0,
        0, 0, 0, 1
    };

    return m * mat;
}
math::vec4 math::rotate(const math::vec4& vec, float pitch, float yaw, float roll) {
    return rotate(math::mat4x4{1}, pitch, yaw, roll) * vec;
}
math::vec4 math::rotate(const math::vec4& vec, float angle, math::vec3 axis) {
    return rotate(math::mat4x4{1}, angle, axis) * vec;
}

math::mat4x4 math::translate(const math::mat4x4& mat, const vec3& v) {
    mat4x4 m {
        1, 0, 0, v.x,
        0, 1, 0, v.y,
        0, 0, 1, v.z,
        0, 0, 0, 1,
    };

    return m * mat;
}
math::vec4 math::translate(const math::vec4& vec, float x, float y, float z) {
    return {
        vec.x + x,
        vec.y + y,
        vec.z + z,
        vec.w
    };
}
math::mat4x4 math::pointAt(const math::vec4& pos, const math::vec4& target, const math::vec4& up) {
    vec4 newForward = target - pos;
    newForward.normalize();

    vec4 a = newForward * (up * newForward);
    vec4 newUp = up - a;
    newUp.normalize();

    vec4 newRight = newUp ^ newForward;

    mat4x4 matrix {
        newRight.x, 	newUp.x,	newForward.x,	pos.x,
        newRight.y, 	newUp.y,	newForward.y,	pos.y,
        newRight.z, 	newUp.z,	newForward.z,	pos.z,
        0,				0,				0,				1
    };
    return matrix;
}

math::mat4x4 math::QuickInverse(const math::mat4x4& m) {
    mat4x4 matrix;
    matrix[0][0] =   m[0][0];  matrix[0][1] = m[1][0];  matrix[0][2] = m[2][0];  matrix[0][3] = 0.0f;
    matrix[1][0] =   m[0][1];  matrix[1][1] = m[1][1];  matrix[1][2] = m[2][1];  matrix[1][3] = 0.0f;
    matrix[2][0] =   m[0][2];  matrix[2][1] = m[1][2];  matrix[2][2] = m[2][2];  matrix[2][3] = 0.0f;
    matrix[3][0] = -(m[3][0] * matrix[0][0] + m[3][1] * matrix[1][0] + m[3][2] * matrix[2][0]);
    matrix[3][1] = -(m[3][0] * matrix[0][1] + m[3][1] * matrix[1][1] + m[3][2] * matrix[2][1]);
    matrix[3][2] = -(m[3][0] * matrix[0][2] + m[3][1] * matrix[1][2] + m[3][2] * matrix[2][2]);
    matrix[3][3] = 1.0f;
    return matrix;
}

