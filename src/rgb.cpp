#include "rgb.h"


const dss::RGBA dss::RGBA::Transparent { 0, 0, 0, 0 };
const dss::RGB dss::RGB::Black 	{  0,   0,   0};
const dss::RGB dss::RGB::Red 		{255,   0,   0};
const dss::RGB dss::RGB::Green 	{  0, 255,   0};
const dss::RGB dss::RGB::Blue 	{  0,   0, 255};
const dss::RGB dss::RGB::Cyan 	{  0, 255, 255};
const dss::RGB dss::RGB::Magenta 	{255,   0, 255};
const dss::RGB dss::RGB::Yellow 	{255, 255,   0};
const dss::RGB dss::RGB::White 	{255, 255, 255};
const dss::RGB dss::RGB::Gray 	{128, 128, 128};
const dss::RGB dss::RGB::Grey 	{192, 192, 192};
const dss::RGB dss::RGB::Maroon 	{128,   0,   0};
const dss::RGB dss::RGB::Darkgreen{  0, 128,   0};
const dss::RGB dss::RGB::Navy 	{  0,   0, 128};
const dss::RGB dss::RGB::Teal 	{  0, 128, 128};
const dss::RGB dss::RGB::Purple 	{128,   0, 128};
const dss::RGB dss::RGB::Olive 	{128, 128,   0};


dss::RGB::RGB() : r{0}, g{0}, b{0} {}
dss::RGB::RGB(uint8_t r, uint8_t g, uint8_t b) : r{r}, g{g}, b{b} {}
dss::RGB::RGB(uint32_t color)
    :   r{static_cast<uint8_t>((color >> 24) & 0xff)},
        g{static_cast<uint8_t>((color >> 16) & 0xff)},
        b{static_cast<uint8_t>((color >>  8) & 0xff)} {}

dss::RGB& dss::RGB::operator+=(const RGB& that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r + that.r, 0, 255));
    this->g = static_cast<uint8_t>(std::clamp(this->g + that.g, 0, 255));
    this->b = static_cast<uint8_t>(std::clamp(this->b + that.b, 0, 255));
    return *this;
}
dss::RGB& dss::RGB::operator-=(const RGB& that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r - that.r, 0, 255));
    this->g = static_cast<uint8_t>(std::clamp(this->g - that.g, 0, 255));
    this->b = static_cast<uint8_t>(std::clamp(this->b - that.b, 0, 255));
    return *this;
}
dss::RGB& dss::RGB::operator*=(const int that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r * that, 0, 255));
    this->g = static_cast<uint8_t>(std::clamp(this->g * that, 0, 255));
    this->b = static_cast<uint8_t>(std::clamp(this->b * that, 0, 255));
    return *this;
}
dss::RGB& dss::RGB::operator/=(const int that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r / that, 0, 255));
    this->g = static_cast<uint8_t>(std::clamp(this->g / that, 0, 255));
    this->b = static_cast<uint8_t>(std::clamp(this->b / that, 0, 255));
    return *this;
}
dss::RGB& dss::RGB::operator*=(const float that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r * that, 0.f, 255.f));
    this->g = static_cast<uint8_t>(std::clamp(this->g * that, 0.f, 255.f));
    this->b = static_cast<uint8_t>(std::clamp(this->b * that, 0.f, 255.f));
    return *this;
}
dss::RGB& dss::RGB::operator/=(const float that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r / that, 0.f, 255.f));
    this->g = static_cast<uint8_t>(std::clamp(this->g / that, 0.f, 255.f));
    this->b = static_cast<uint8_t>(std::clamp(this->b / that, 0.f, 255.f));
    return *this;
}
bool dss::RGB::operator==(const RGB& that) const {
    return this->r == that.r && this->g == that.g && this->b == that.b;
}
bool dss::RGB::operator!=(const RGB& that) const {
    return !(*this == that);
}
dss::RGB dss::RGB::operator+(const RGB& that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r + that.r, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->g + that.g, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->b + that.b, 0, 255)),
    };
}
dss::RGB dss::RGB::operator-(const RGB& that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r - that.r, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->g - that.g, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->b - that.b, 0, 255)),
    };
}
dss::RGB dss::RGB::operator*(const int that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r * that, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->g * that, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->b * that, 0, 255)),
    };
}
dss::RGB dss::RGB::operator/(const int that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r / that, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->g / that, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->b / that, 0, 255)),
    };
}
dss::RGB dss::RGB::operator*(const float that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->g * that, 0.f, 255.f)),
        static_cast<uint8_t>(std::clamp(this->b * that, 0.f, 255.f)),
        static_cast<uint8_t>(std::clamp(this->r * that, 0.f, 255.f)),
    };
}
dss::RGB dss::RGB::operator/(const float that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r / that, 0.f, 255.f)),
        static_cast<uint8_t>(std::clamp(this->g / that, 0.f, 255.f)),
        static_cast<uint8_t>(std::clamp(this->b / that, 0.f, 255.f)),
    };
}


dss::RGBA::RGBA() : dss::RGB(), a(0xff) {
}
dss::RGBA::RGBA(uint8_t r, uint8_t g, uint8_t b, uint8_t a) : dss::RGB(r, g, b), a(a) {
}
dss::RGBA::RGBA(uint32_t color) : RGB(color), a{static_cast<uint8_t>(color & 0xff)} {
}
dss::RGBA::RGBA(const dss::RGB& rgb, int a) : dss::RGB(rgb), a(a) {

}

dss::RGBA& dss::RGBA::operator+=(const RGBA& that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r + that.r, 0, 255));
    this->g = static_cast<uint8_t>(std::clamp(this->g + that.g, 0, 255));
    this->b = static_cast<uint8_t>(std::clamp(this->b + that.b, 0, 255));
    return *this;
}
dss::RGBA& dss::RGBA::operator-=(const RGBA& that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r - that.r, 0, 255));
    this->g = static_cast<uint8_t>(std::clamp(this->g - that.g, 0, 255));
    this->b = static_cast<uint8_t>(std::clamp(this->b - that.b, 0, 255));
    return *this;
}
dss::RGBA& dss::RGBA::operator*=(const int that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r * that, 0, 255));
    this->g = static_cast<uint8_t>(std::clamp(this->g * that, 0, 255));
    this->b = static_cast<uint8_t>(std::clamp(this->b * that, 0, 255));
    return *this;
}
dss::RGBA& dss::RGBA::operator/=(const int that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r / that, 0, 255));
    this->g = static_cast<uint8_t>(std::clamp(this->g / that, 0, 255));
    this->b = static_cast<uint8_t>(std::clamp(this->b / that, 0, 255));
    return *this;
}

dss::RGBA& dss::RGBA::operator*=(const float that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r * that, 0.f, 255.f));
    this->g = static_cast<uint8_t>(std::clamp(this->g * that, 0.f, 255.f));
    this->b = static_cast<uint8_t>(std::clamp(this->b * that, 0.f, 255.f));
    return *this;
}
dss::RGBA& dss::RGBA::operator/=(const float that) {
    this->r = static_cast<uint8_t>(std::clamp(this->r / that, 0.f, 255.f));
    this->g = static_cast<uint8_t>(std::clamp(this->g / that, 0.f, 255.f));
    this->b = static_cast<uint8_t>(std::clamp(this->b / that, 0.f, 255.f));
    return *this;
}
bool dss::RGBA::operator==(const RGBA& that) const {
    return this->r == that.r && this->g == that.g && this->b == that.b && this->a == that.a;
}
bool dss::RGBA::operator!=(const RGBA& that) const {
    return !(*this == that);
}
dss::RGBA dss::RGBA::operator+(const RGBA& that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r + that.r, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->g + that.g, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->b + that.b, 0, 255)),
        this->a,
    };
}
dss::RGBA dss::RGBA::operator-(const RGBA& that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r - that.r, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->g - that.g, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->b - that.b, 0, 255)),
        this->a,
    };
}
dss::RGBA dss::RGBA::operator*(const int that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r * that, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->g * that, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->b * that, 0, 255)),
        this->a,
    };
}
dss::RGBA dss::RGBA::operator/(const int that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r / that, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->g / that, 0, 255)),
        static_cast<uint8_t>(std::clamp(this->b / that, 0, 255)),
        this->a,
    };
}
dss::RGBA dss::RGBA::operator*(const float that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r * that, 0.f, 255.f)),
        static_cast<uint8_t>(std::clamp(this->g * that, 0.f, 255.f)),
        static_cast<uint8_t>(std::clamp(this->b * that, 0.f, 255.f)),
        this->a,
    };
}
dss::RGBA dss::RGBA::operator/(const float that) const {
    return {
        static_cast<uint8_t>(std::clamp(this->r / that, 0.f, 255.f)),
        static_cast<uint8_t>(std::clamp(this->g / that, 0.f, 255.f)),
        static_cast<uint8_t>(std::clamp(this->b / that, 0.f, 255.f)),
        this->a,
    };
}

uint32_t dss::ToColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    return (r << 24) + (g << 16) + (b << 8) + (a);
}
